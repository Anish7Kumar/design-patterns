package com.creational.patterns;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * It is a creational design pattern.
 * Main objective is that only one instance of the class should exists throughout 
 * Examples: Session, Cache, Logging, Drivers
 * @author bunnu
 *
 */

public class Singleton {

	public static void main(String[] args) throws IOException {
		
		Logger log = Logger.get();
		
		log.info("I am info from log : "+log);
		log.error("I am error from log : "+log);
		
		Logger log1 = Logger.getSynchronized();
		System.out.println(log1);
		log1.info("I am info from log1 : "+log1);
		log1.error("I am error from log1 : "+log1);

	}

}

class Logger
{
	private static Logger log = null;
	
	File loggingFileDir ;
	File loggingFile;
	BufferedWriter wr;
	private Logger() throws IOException
	{
		loggingFileDir = new File(System.getProperty("user.dir")+File.separator+"logs");
		loggingFileDir.mkdirs();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mmm-dd");
		loggingFile = new File(
				System.getProperty("user.dir")+File.separator+"logs"+File.separator+sdf.format(date)+"_Logger.log"
				);
		
			loggingFile.createNewFile();
			wr = new BufferedWriter(new FileWriter(loggingFile));
		
	}
	
	public void info(String message) 
	{
		String currTime = new Date().toString();
		String classCaller = Thread.currentThread().getStackTrace()[2].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String fileName = Thread.currentThread().getStackTrace()[2].getFileName();
		String log = currTime+" >"+fileName+" "+classCaller+" "+methodName+" : "+message;
		System.out.println(log);
		write(log);	
	}
	
	public void error(String message)
	{
		String currTime = new Date().toString();
		String classCaller = Thread.currentThread().getStackTrace()[2].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String fileName = Thread.currentThread().getStackTrace()[2].getFileName();
		
		String log = currTime+" >"+fileName+" "+classCaller+" "+methodName+" : "+message;
		System.err.println(log);
		write(log);
	}
	
	
	public void error(String message,Throwable e)
	{
		String currTime = new Date().toString();
		String classCaller = Thread.currentThread().getStackTrace()[2].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String fileName = Thread.currentThread().getStackTrace()[2].getFileName();
		String log = currTime+" >"+fileName+" "+classCaller+" "+methodName+" : "+message+" , "+e.getMessage();
		System.err.println(log);
		write(log);
		
	}
	
	
	private void write(String log)
	{
		try {
			wr.write(log);
			wr.flush();
		} catch (IOException e) {
			
			System.err.println("Error while logging due to "+e.getMessage());
		}
	}
	
	//Lazy Instantiation
	public static Logger get() throws IOException
	{
		if(log==null)
			log = new Logger();
		
		return log;
	}
	
	//synchronized block
	public static Logger getSynchronized() throws IOException
	{
			synchronized (Logger.class) {
				if(log==null)
				{
					log = new Logger();
				}
			}
		
		return log;
	}
	
	
}
